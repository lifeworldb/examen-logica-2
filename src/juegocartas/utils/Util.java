package juegocartas.utils;

import java.lang.reflect.Array;

public class Util {

    // Metodo para redimensionar vectores
    public static Object redimensionar(Object arregloOriginal, int nuevoTamano) {
        Object nuevoArreglo = null;
        if (arregloOriginal != null) {
// Determina el tipo de cada elemento del arreglo
            Class tipo = arregloOriginal.getClass();
            Class t = tipo.getComponentType();
// Construye un nuevo arreglo con el nuevo numero de elementos
// Cada elemento es del mismo tipo del arreglo original
            nuevoArreglo = Array.newInstance(t, nuevoTamano);
// Copia el elemento desde el arreglo original en el nuevo arreglo
            System.arraycopy(arregloOriginal, 0, nuevoArreglo, 0, Math.min(Array.getLength(arregloOriginal), nuevoTamano));
        }
// retorna el nuevo arreglo
        return nuevoArreglo;
    }

}
