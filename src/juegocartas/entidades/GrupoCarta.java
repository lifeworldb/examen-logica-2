
package juegocartas.entidades;

import juegocartas.enums.Grupo;
import juegocartas.enums.NombreCarta;


public class GrupoCarta {
    
    private NombreCarta nombre;
    private Grupo grupo;
    
    
    public GrupoCarta(NombreCarta nombre, Grupo grupo){
        this.nombre=nombre;
        this.grupo=grupo;
                
    }
    public String obtenerTexto(){
        return grupo.name()+ " de " +nombre.name();
    }
    
}
