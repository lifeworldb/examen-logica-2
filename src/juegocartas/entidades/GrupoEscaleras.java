/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegocartas.entidades;

import java.util.*;
import juegocartas.enums.Pinta;

/**
 *
 * @author Home
 */
public class GrupoEscaleras {
    private int indiceNombre;
    private Pinta pinta;
    
    //ArrayList<GrupoEscaleras>[] newGrupo = new ArrayList[4];
    
    public GrupoEscaleras(int nombre, Pinta pinta){
        /*for (int i = 0; i < newGrupo.length; i++) {
            newGrupo[i] = new ArrayList();
        }*/
        this.indiceNombre = nombre;
        this.pinta = pinta;
    }

    public int getIndiceNombre() {
        return indiceNombre;
    }

    public Pinta getPinta() {
        return pinta;
    }
    
    @Override
    public String toString(){
        return String.format("%s %s", indiceNombre, pinta);
    }
    
    public String toString(int s){
        return String.format("Se encontro una escalera de %s cartas de %s", s, pinta);
    }
    
    public static List odernar(ArrayList<GrupoEscaleras>[] m, int size){
        List colList = new ArrayList<GrupoEscaleras>();
        
        m[size].forEach((n) -> {
            colList.add(n);
        });
        
        Collections.sort(colList, new Sortbyindexname());
        
        return colList;
    }
    
}

class Sortbyindexname implements Comparator<GrupoEscaleras> {
    public int compare(GrupoEscaleras a, GrupoEscaleras b) {
        return a.getIndiceNombre() - b.getIndiceNombre();
    }
}
