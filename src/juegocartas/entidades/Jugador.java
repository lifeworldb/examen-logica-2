package juegocartas.entidades;

import juegocartas.enums.Grupo;
import juegocartas.utils.Util;
import java.util.*;
import javax.swing.*;
import juegocartas.enums.NombreCarta;
import juegocartas.enums.Pinta;

public class Jugador {

    Carta[] cartas;

    public void repartir() {
        cartas = new Carta[10];

        for (int i = 0; i < cartas.length; i++) {
            cartas[i] = new Carta();

        }
    }

    public void mostrar(JPanel pnl, boolean tapada) {
        pnl.removeAll();
        for (int i = 0; i < cartas.length; i++) {
            cartas[i].mostrarCarta(10 + i * 30, 10, pnl, tapada);
        }

        pnl.repaint();
    }

    public ArrayList<GrupoEscaleras>[] ObtenerEscaleras() {
        GrupoEscaleras[] grupo = new GrupoEscaleras[cartas.length];
        ArrayList<GrupoEscaleras>[] grupoEM = new ArrayList[4];

        for (int i = 0; i < grupoEM.length; i++) {
            grupoEM[i] = new ArrayList();
        }

        for (int i = 0; i < cartas.length; i++) {
            grupo[i] = new GrupoEscaleras(cartas[i].obtenerIndiceNombre(), cartas[i].obtenerPinta());
        }

        for (int j = 0; j < grupo.length; j++) {
            if (grupo[j].getPinta() == Pinta.TREBOL) {
                grupoEM[Pinta.TREBOL.ordinal()].add(grupo[j]);
                //grupoM[Pinta.TREBOL.ordinal()][j] = grupo[j];
            }

            if (grupo[j].getPinta() == Pinta.CORAZON) {
                //grupoM[Pinta.CORAZON.ordinal()][j] = grupo[j];
                grupoEM[Pinta.CORAZON.ordinal()].add(grupo[j]);
            }

            if (grupo[j].getPinta() == Pinta.DIAMANTE) {
                //grupoM[Pinta.DIAMANTE.ordinal()][j] = grupo[j];
                grupoEM[Pinta.DIAMANTE.ordinal()].add(grupo[j]);
            }

            if (grupo[j].getPinta() == Pinta.PICA) {
                //grupoM[Pinta.PICA.ordinal()][j] = grupo[j];
                grupoEM[Pinta.PICA.ordinal()].add(grupo[j]);
            }
        }

        for (int i = 0; i < 4; i++) {
            grupoEM[i] = (ArrayList<GrupoEscaleras>) GrupoEscaleras.odernar(grupoEM, i);
            System.out.printf("Matriz ordenada: %s", GrupoEscaleras.odernar(grupoEM, i));
            System.out.println("");
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < grupoEM[i].size() - 1; j++) {
                if (grupoEM[i].size() > 2) {
                    if ((grupoEM[i].get(j + 1).getIndiceNombre() - grupoEM[i].get(j).getIndiceNombre()) != 1 && (grupoEM[i].get(j).getIndiceNombre() != 0)) {
                        grupoEM[i].remove(j);
                    }
                }

            }
        }

        ArrayList<GrupoEscaleras>[] ge = new ArrayList[4];

        for (int i = 0; i < 4; i++) {
            if (grupoEM[i].size() > 2) {
                ge[i] = grupoEM[i];
            }

        }
        return ge;
    }

    public GrupoCarta[] obtenerGrupos() {
        int[] contadores = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,};

        for (int i = 0; i < cartas.length; i++) {
            contadores[cartas[i].obtenerIndiceNombre()]++;
        }

        GrupoCarta[] gc = null;
        for (int i = 0; i < contadores.length; i++) {
            if (contadores[i] > 1) {
                GrupoCarta g = new GrupoCarta(NombreCarta.values()[i], Grupo.values()[contadores[i]]);

                if (gc == null) {
                    gc = new GrupoCarta[1];
                } else {
                    gc = (GrupoCarta[]) Util.redimensionar(gc, gc.length + 1);
                }

                gc[gc.length - 1] = g;
            }

        }
        return gc;
    }
}
