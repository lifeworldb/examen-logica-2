package juegocartas.entidades;

import java.util.*;
import javax.swing.*;
import juegocartas.enums.NombreCarta;
import juegocartas.enums.Pinta;

public class Carta {

    /* Atributo privado para almacenar el numero de la carta
     * 1= As de Trebol, 14= As de Pica, 27= As de Corazon, 40= As de Diamante,
     * 2= 2 de Trebol, 15= 2 de Pica, 28= 2 de Corazon, 41= 2 de Diamante,
     * 3= 3 de Trebol, 16= 3 de Pica, 29= 3 de Corazon, 42= 3 de Diamante,
     * ...
     * 10= 10 de Trebol, 23= 10 de Pica, 36= 10 de Corazon, 49= 10 de Diamante,
     * 11= J de Trebol, 24= J de Pica, 37= J de Corazon, 50= J de Diamante,
     * 12= Q de Trebol, 25= Q de Pica, 38= Q de Corazon, 51= Q de Diamante,
     * 13= K de Trebol, 26= K de Pica, 39= K de Corazon, 52= K de Diamante
     * */
    private int indice;
    //METODO CONSTRUCTOR
    public Carta()
    {
        //El numero de la carta es generado aleatoriamente
		Random r=new Random();
        indice=r.nextInt(52)+1;
    }
    
    public Pinta obtenerPinta()
            
    {
        /* Obtiene la pinta que corresponde a la carta,
         * basado en el rango que se ubica el índice
         */
        if(indice<=13)
            return Pinta.TREBOL;
        else if (indice<=26)
            return Pinta.PICA;
        else if (indice<=39)
            return Pinta.CORAZON;
        else
            return Pinta.DIAMANTE;
    }
    
    public NombreCarta obtenerNombre()
    {
        //Obtiene el nombre que corresponde al numero de la carta
        int numero = indice % 13;
        if (numero == 0) {
            numero = 13;
        }
        return NombreCarta.values()[numero];
    }
    
    public void mostrarCarta(int x, int y, JPanel pnl, boolean tapada)
    {
        String nombreImagen;
        //Obtener el nombre del archivo de la carta
        if(tapada)
            nombreImagen = "/Cartas/Tapada.jpg";
        else
            nombreImagen = "/Cartas/Carta" + String.valueOf(indice) + ".JPG";

        //Cargar la imagen
        ImageIcon imagen = new ImageIcon(getClass().getResource(nombreImagen));
        //Instanciar label para mostrar la imagen
        JLabel lblCarta = new JLabel(imagen);
        //Definir posicion y dimensiones de la imagen
        lblCarta.setBounds(x, y, 
                            x + imagen.getIconWidth(), 
                            y + imagen.getIconHeight());
        //Mostrar la carta en la ventana
        pnl.add(lblCarta);
    }
    public int obtenerIndiceNombre()
    {
        int numero= indice % 13;
        if(numero==0)
        
           return 12;
    
        else
        
            return numero-1;
    }
}
